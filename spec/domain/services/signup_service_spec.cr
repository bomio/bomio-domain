require "../../spec_helper"

class FakeSignupOutput < SignupOutput
end

class FakeUserRepository < UserRepository
  getter users : Array(User) = Array(User).new

  def save(user : User)
    @users << user
  end
end

describe SignupService do
  it "initialize with a repository and an output" do
    repository = FakeUserRepository.new
    output = FakeSignupOutput.new
    service = SignupService.new(repository, output)
    service.repository.should be_a UserRepository
    service.output.should be_a SignupOutput
  end

  it "create an new user in persistence" do
    repository = FakeUserRepository.new
    output = FakeSignupOutput.new
    service = SignupService.new(repository, output)

    service.register("Captain", "Hadock")
    repository.users.first.firstname.should eq "Captain"
  end
end
