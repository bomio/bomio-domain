require "uuid"

class User
  getter firstname : String
  getter lastname : String
  getter id : UUID
  getter moods : Array(Mood) = Array(Mood).new

  def initialize(@firstname, @lastname)
    @id = UUID.v4
  end

  def complete_name
    "%s %s" % [@firstname, @lastname]
  end
end

enum MoodLevel
  Sad
  Neutral
  Happy
end

class Mood
  getter date : Time
  getter level : MoodLevel
  getter user : User

  def initialize(date, level, @user)
    @date = Time.parse(date, "%Y/%m/%d", Time::Location::UTC)
    @level = MoodLevel.parse(level)
  end
end
