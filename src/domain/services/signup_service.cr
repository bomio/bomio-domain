require "../ports/user_repository"
require "../ports/signup_output"

class SignupService
  getter repository : UserRepository
  getter output : SignupOutput

  def initialize(@repository, @output)
  end

  def register(firstname : String, lastname : String)
    repository.save(User.new(firstname, lastname))
  end
end
