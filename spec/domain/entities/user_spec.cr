require "../../spec_helper"

describe User do
  it "initialize wuth firstname, lastname and without id " do
    user = User.new("Captain", "Hadock")
    user.firstname.should eq "Captain"
    user.lastname.should eq "Hadock"
    user.id.should be_a UUID
  end

  it "has a complete name" do
    user = User.new("Tintin", "Milou")
    user.complete_name.should eq "Tintin Milou"
  end

  it "can add a mood" do
    user = User.new("Tintin", "Milou")
    mood = Mood.new("2024/04/07", "happy", user)
    user.moods << mood
    user.moods.size.should eq 1
    user.moods.first.level.should eq MoodLevel::Happy
  end
end

describe Mood do
  it "initialize with date, level, user" do
    user = User.new("Captain", "Hadock")
    mood = Mood.new("2024/04/07", "happy", user)
    mood.date.should be_a Time
    mood.date.should eq Time.parse("2024/04/07", "%Y/%m/%d", Time::Location::UTC)
    mood.level.should be_a MoodLevel
    mood.level.should eq MoodLevel::Happy
    mood.user.should eq user
  end
end
