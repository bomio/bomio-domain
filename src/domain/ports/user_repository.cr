abstract class UserRepository
  abstract def users : Array(User)
  abstract def save(user : User)
end
